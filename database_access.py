from flask import Flask
from flask.ext.mysql import MySQL

app = Flask(__name__)
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'nico'
app.config['MYSQL_DATABASE_DB'] = 'testdb'
mysql = MySQL(app)


@app.route('/')
def index():
    cur = mysql.get_db().cursor()
    cur.execute('''SELECT data FROM test_table WHERE id = 564 ''')
    rv = cur.fetchall()
    return str(rv)

if __name__ == '__main__':
    app.run(debug=True)
