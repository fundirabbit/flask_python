from flask import Flask, render_template

app = Flask(__name__)


@app.route("/", methods=['GET'])
def index():
    
    names = {'name': 'Rebeca', 'Age': '29'}
    items = [{'text': 'First'}, {'text': 'Second'}, {'text': 'Third'}]

    return render_template('layout.html', name=names,items=items, lang= False, framework='flask', language="Python")


if __name__ == '__main__':
    app.run(debug=True)
